package com.epam.controller;

import com.epam.model.*;

/**
 * Class contains methods for proper working of controller.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class ControllerIml implements Controller {

    @Override
    public void pingPong() {
        PingPong p=new PingPong();
        p.startTennis();
    }

    @Override
    public void startFibonacci() {
        FibonacciThreads f=new FibonacciThreads();
        f.show();
    }

    @Override
    public void executorFibonacci() {
        ExecutorsExample executorsExample=new ExecutorsExample();
        executorsExample.testExecutor();
    }

    @Override
    public void callableFibonacci() throws InterruptedException {
        ScheduleExample callableExample =new ScheduleExample();
        callableExample.testSchedule();
    }

    @Override
    public void readWriteTest() {
        ReadWriteLockTest.test();

    }
}
