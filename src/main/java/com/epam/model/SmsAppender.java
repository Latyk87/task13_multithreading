package com.epam.model;

import java.io.Serializable;

import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.core.layout.PatternLayout;

/**
 * Class creates the sms for fatal problems in following program.
 * Created by Borys Latyk on 21/11/2019.
 *
 * @version 2.1
 * @since 21.11.2019
 */

@Plugin(name = "SMS", category = "Core", elementType = "appender", printObject = true)
public final class SmsAppender extends AbstractAppender {
    protected SmsAppender(String name, Filter filter,
                          Layout<? extends Serializable> layout, final boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
    }

    @Override
    public void append(LogEvent event) {
        try {
            ExampleSMS.send(new String(getLayout().toByteArray(event)));
        } catch (Exception ex) {
        }
    }

    @PluginFactory
    public static SmsAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter,
            @PluginAttribute("otherAttribute") String otherAttribute) {
        if (name == null) {
            LOGGER.fatal("No name provided for MyCustomAppenderImpl");
            return null;
        }
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new SmsAppender(name, filter, layout, true);
    }
}
