package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Class shows the example of ScheduledExecutorService Interface.
 * Created by Borys Latyk on 08/12/2019.
 *
 * @version 2.1
 * @since 08.12.2019
 */
public class ScheduleExample {
    private static Logger logger1 = LogManager.getLogger(Application.class);

    public void testSchedule() throws InterruptedException {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        Runnable task = () -> logger1.info(" Sleeping time " + System.nanoTime());
        ScheduledFuture<?> future = executor.schedule(task, 5, TimeUnit.SECONDS);
        TimeUnit.MILLISECONDS.sleep(2000);
        long remainingDelay = future.getDelay(TimeUnit.MILLISECONDS);
        System.out.printf("Remaining Delay: %sms", remainingDelay);
    }
}
