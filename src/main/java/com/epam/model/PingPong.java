package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;

/**
 * Class handles two threads using wait() and notify().
 * Created by Borys Latyk on 08/12/2019.
 *
 * @version 2.1
 * @since 08.12.2019
 */
public class PingPong {
    private volatile static long A = 0;
    private static Object obj = new Object();
    private static Logger logger1 = LogManager.getLogger(Application.class);

    public void startTennis() {
        Thread t1 = new Thread(() -> {
            synchronized (obj) {
                for (int i = 0; i < 10000000; i++) {
                    try {
                        obj.wait();
                    } catch (InterruptedException e) {
                    }
                    A++;
                    obj.notify();
                }
                logger1.info("finish " + Thread.currentThread().getName());
            }
        });
        Thread t2 = new Thread(() -> {
            synchronized (obj) {
                for (int i = 0; i < 10000000; i++) {
                    obj.notify();
                    try {
                        obj.wait();
                    } catch (InterruptedException e) {
                    }
                    A++;
                }
                logger1.info("Finish " + Thread.currentThread().getName());
            }
        });

        logger1.info(LocalDateTime.now());
        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
        }

        logger1.info(LocalDateTime.now());
        logger1.info("A=" + A);
    }
}
