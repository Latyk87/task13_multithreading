package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Random;
/**
 * This thread randomly read an element from a shared data structure
 * Created by Borys Latyk on 11/12/2019.
 *
 * @version 2.1
 * @since 11.12.2019
 */
public class Writer extends  Thread {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private ReadWriteList<Integer> sharedList;

    public Writer(ReadWriteList<Integer> sharedList) {
        this.sharedList = sharedList;
    }

    public void run() {
        Random random = new Random();
        int number = random.nextInt(100);
        sharedList.add(number);

        try {
            Thread.sleep(100);
            logger1.info(getName() + " -> put: " + number);
        } catch (InterruptedException ie ) { ie.printStackTrace(); }
    }
}
