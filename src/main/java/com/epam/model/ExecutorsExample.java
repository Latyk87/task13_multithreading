package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Class produces a sequence of n Fibonacci numbers using Executor interface.
 * Created by Borys Latyk on 08/12/2019.
 *
 * @version 2.1
 * @since 08.12.2019
 */
public class ExecutorsExample {
    private static Logger logger1 = LogManager.getLogger(Application.class);

    public void testExecutor() {
        FibonacciThreads fibonacciThreads = new FibonacciThreads();
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(() -> {
            List<Long> lex = Arrays.asList(fibonacciThreads.findFibonacciset
                    (new FibonacciThreads(35)));
            logger1.info(lex);
        });

        executorService.submit(() -> {
            List<Long> lex = Arrays.asList(fibonacciThreads.findFibonacciset
                    (new FibonacciThreads(45)));
            logger1.info(lex);
        });

        executorService.submit(() -> {
            List<Long> lex = Arrays.asList(fibonacciThreads.findFibonacciset
                    (new FibonacciThreads(55)));
            logger1.info(lex);
        });

        executorService.submit(() -> {
            List<Long> lex = Arrays.asList(fibonacciThreads.findFibonacciset
                    (new FibonacciThreads(65)));
            logger1.info(lex);
        });

        executorService.submit(() -> {
            List<Long> lex = Arrays.asList(fibonacciThreads.findFibonacciset
                    (new FibonacciThreads(75)));
            logger1.info(lex);
        });

        executorService.submit(() -> {
            List<Long> lex = Arrays.asList(fibonacciThreads.findFibonacciset
                    (new FibonacciThreads(30)));
            logger1.info(lex);
        });

        executorService.shutdown();
    }


}
