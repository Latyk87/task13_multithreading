package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

/**
 * Class produces a sequence of n Fibonacci numbers using threads.
 * Created by Borys Latyk on 08/12/2019.
 *
 * @version 2.1
 * @since 08.12.2019
 */
public class FibonacciThreads implements Runnable {
    private volatile int fibonacciset;
    private static Logger logger1 = LogManager.getLogger(Application.class);

    public FibonacciThreads() {
    }

    public FibonacciThreads(int fibonacciset) {
        this.fibonacciset = fibonacciset;
    }

    public synchronized Long[] findFibonacciset(FibonacciThreads o) {
        Long[] fibonnaci = new Long[o.getFibonacciset()];

        long numFirst = 0;
        long numSecond = 1;
        long numFinal;

        for (int i = 0; i < 2; i++) {
            if (i == 0) {
                fibonnaci[0] = numFirst;
            } else if (i == 1) {
                fibonnaci[1] = numSecond;
            }
        }
        for (int i = 2; i < fibonnaci.length; i++) {
            numSecond = numFirst + numSecond;
            numFirst = numSecond - numFirst;
            numFinal = numFirst + numSecond;
            fibonnaci[i] = numFinal;
        }
        return fibonnaci;
    }

    public void show() {
        Thread t1 = new Thread(new FibonacciThreads(50));
        Thread t2 = new Thread(new FibonacciThreads(60));
        Thread t3 = new Thread(new FibonacciThreads(70));
        Thread t4 = new Thread(new FibonacciThreads(80));
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int getFibonacciset() {
        return fibonacciset;
    }

    @Override
    public void run() {
        Long[] a = findFibonacciset(this);
        List<Long> in = Arrays.asList(a);
        logger1.info(in);
    }
}
