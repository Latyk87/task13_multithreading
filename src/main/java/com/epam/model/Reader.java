package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Random;
/**
 * This thread randomly adds an element to a shared data structure
 * Created by Borys Latyk on 11/12/2019.
 *
 * @version 2.1
 * @since 11.12.2019
 */
public class Reader extends Thread {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private ReadWriteList<Integer> sharedList;

    public Reader(ReadWriteList<Integer> sharedList) {
        this.sharedList = sharedList;
    }

    public void run() {
        Random random = new Random();
        int index = random.nextInt(sharedList.size());
        Integer number = sharedList.get(index);

        logger1.info(getName() + " -> get: " + number);

        try {
            Thread.sleep(100);
        } catch (InterruptedException ie ) { ie.printStackTrace(); }

    }
}
