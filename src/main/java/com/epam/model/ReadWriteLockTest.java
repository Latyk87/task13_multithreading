package com.epam.model;

/**
 * Test program for understanding ReadWriteLock
 * Created by Borys Latyk on 11/12/2019.
 *
 * @version 2.1
 * @since 11.12.2019
 */
public class ReadWriteLockTest {
    static final int READER_SIZE = 10;
    static final int WRITER_SIZE = 2;

    public static void test() {
        Integer[] initialElements = {33, 28, 86, 99};

        ReadWriteList<Integer> sharedList = new ReadWriteList<>(initialElements);

        for (int i = 0; i < WRITER_SIZE; i++) {
            new Writer(sharedList).start();
        }

        for (int i = 0; i < READER_SIZE; i++) {
            new Reader(sharedList).start();
        }

    }
}
